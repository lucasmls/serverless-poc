const elasticsearch = require("elasticsearch");

const esClient = new elasticsearch.Client({
  apiVersion: "7.1",
  host:
    "https://vpc-es-nanoservices-cluster-mlojebhomyz3kofeyz7nu5cxru.us-east-1.es.amazonaws.com"
});

const search = async query => {
  return await esClient.search({
    index: "images",
    q: "tags: " + query
  });
};

module.exports = { search };
