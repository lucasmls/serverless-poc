const AWS = require("aws-sdk");
const uuid = require("uuid/v4");

AWS.config.update({
  region: "us-east-1"
});

const S3 = new AWS.S3();

const BUCKET_NAME = "nanoservices-bucket";

const upload = body => {
  return new Promise((resolve, reject) => {
    const id = uuid();

    S3.putObject(
      {
        Bucket: BUCKET_NAME,
        Key: id + ".jpg",
        Body: new Buffer(body.replace(/^data:image\/w+;base64,/, ""), "base64"),
        ContentEncoding: "base64",
        ContentType: "image/jpeg"
      },

      err => {
        if (err) {
          return reject(err);
        }

        return resolve({
          bucket: BUCKET_NAME,
          key: id + ".jpg"
        });
      }
    );
  });
};

module.exports = { upload };
