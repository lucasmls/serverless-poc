"use strict";

const s3Service = require("./services/s3");
const dynamodbService = require("./services/dynamodb");
const elasticsearchService = require("./services/elasticsearch");

module.exports.upload = async event => {
  const item = await s3Service.upload(event.body);

  await dynamodbService.put(item);

  return {
    statusCode: 201,
    body: JSON.stringify(item)
  };
};

module.exports.get = async event => {
  const tags = await elasticsearchService.search(event.queryStringParameters.q);

  return {
    statusCode: 201,
    body: JSON.stringify(tags)
  };
};
