const jimp = require("jimp");

const s3Service = require("./s3");
const sqsService = require("./sqs");

const thumbnail = async event => {
  const {
    Records: [
      {
        s3: {
          bucket: { name: bucketName },
          object: { key }
        }
      }
    ]
  } = JSON.parse(event.Records[0].Sns.Message);

  const s3Object = await s3Service.getObject(bucketName, key);
  const image = await jimp.read(s3Object);

  const buffer = await image
    .resize(100, 100)
    .quality(80)
    .getBufferAsync(jimp.MIME_JPEG);

  const thumbnailData = await s3Service.putObject(buffer, key);

  await sqsService.putMessage({
    ...thumbnailData,
    eventType: "THUMBNAIL_EVENT"
  });
};

module.exports = { thumbnail };
