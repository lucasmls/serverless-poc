"use strict";

const thumbnailService = require("./services/thumbnail");

module.exports.thumbnail = async event => {
  console.log("Evento do SQs recebido com sucesso!", JSON.stringify(event));

  await thumbnailService.thumbnail(event);

  return {
    message: "Thumbnail generated successfully!",
    event
  };
};
