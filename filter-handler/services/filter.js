const jimp = require("jimp");

const s3Service = require("./s3");
const sqsService = require("./sqs");

const filter = async event => {
  const {
    Records: [
      {
        s3: {
          bucket: { name: bucketName },
          object: { key }
        }
      }
    ]
  } = JSON.parse(event.Records[0].Sns.Message);

  const s3Object = await s3Service.getObject(bucketName, key);
  const image = await jimp.read(s3Object);

  const buffer = await image
    .greyscale()
    .quality(80)
    .getBufferAsync(jimp.MIME_JPEG);

  const filterData = await s3Service.putObject(buffer, key);

  await sqsService.putMessage({
    ...filterData,
    eventType: "FILTER_EVENT"
  });
};

module.exports = { filter };
