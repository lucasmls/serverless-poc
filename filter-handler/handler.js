"use strict";

const filterService = require("./services/filter");

module.exports.filter = async event => {
  console.log("Evento do SQs recebido com sucesso!", JSON.stringify(event));

  await filterService.filter(event);

  return {
    message: "Image filtered to black and white successfully!",
    event
  };
};
