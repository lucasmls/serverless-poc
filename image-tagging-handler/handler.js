"use strict";

const rekognitionService = require("./services/rekognition");
const sqsService = require("./services/sqs");

module.exports.tagging = async event => {
  const {
    Records: [
      {
        s3: {
          bucket: { name: bucketName },
          object: { key }
        }
      }
    ]
  } = JSON.parse(event.Records[0].Sns.Message);

  const labels = await rekognitionService.detectLabels(bucketName, key);

  console.log("Will add the tag event into SQS.");
  await sqsService.putMessage({
    key,
    labels,
    eventType: "TAG_EVENT"
  });

  return {
    message: "Your function executed successfully!",
    event
  };
};
