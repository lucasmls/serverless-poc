const AWS = require("aws-sdk");

AWS.config.update({
  region: "us-east-1"
});

const rekognition = new AWS.Rekognition();

const detectLabels = (bucket, key) => {
  return new Promise((resolve, reject) => {
    rekognition.detectLabels(
      {
        Image: {
          S3Object: {
            Bucket: bucket,
            Name: key
          }
        },
        MinConfidence: 80,
        MaxLabels: 6
      },
      (error, data) => {
        if (error) {
          return reject(error);
        }

        console.log(data);
        return resolve(data.Labels.map(label => label.Name));
      }
    );

    console.log(`Labels of the image ${key} from bucket ${bucket}`);
  });
};

module.exports = { detectLabels };
