const AWS = require("aws-sdk");

AWS.config.update({
  region: "us-east-1"
});

const SQS = new AWS.SQS();

const putMessage = message => {
  return new Promise((resolve, reject) => {
    SQS.sendMessage(
      {
        QueueUrl:
          "https://sqs.us-east-1.amazonaws.com/092621869745/post-processing-image-queue	",
        MessageBody: JSON.stringify(message)
      },
      (error, data) => {
        if (error) {
          return reject(error);
        }

        return resolve(data);
      }
    );
  });
};

module.exports = { putMessage };
