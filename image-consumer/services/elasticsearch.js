const elasticsearch = require("elasticsearch");

const esClient = new elasticsearch.Client({
  apiVersion: "7.1",
  host:
    "https://vpc-es-nanoservices-cluster-mlojebhomyz3kofeyz7nu5cxru.us-east-1.es.amazonaws.com"
});

const index = async document => {
  return await esClient.index({
    index: "images",
    type: "object",
    body: document
  });
};

module.exports = { index };
