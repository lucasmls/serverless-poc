const AWS = require("aws-sdk");

AWS.config.update({
  region: "us-east-1"
});

const dynamodb = new AWS.DynamoDB.DocumentClient();

const TABLE_NAME = "images";

const putItem = item => {
  return new Promise((resolve, reject) => {
    dynamodb.put(
      {
        TableName: TABLE_NAME,
        Item: item
      },
      (err, data) => {
        if (err) {
          return reject(err);
        }
        return resolve(data);
      }
    );
  });
};

const getItem = id => {
  return new Promise((resolve, reject) => {
    dynamodb.get(
      {
        TableName: TABLE_NAME,
        Key: {
          id
        }
      },
      (error, data) => {
        if (error) {
          return reject(error);
        }

        return resolve(data.Item);
      }
    );
  });
};

module.exports = { putItem, getItem };
